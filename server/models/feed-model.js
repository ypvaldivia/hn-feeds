const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Feed = new Schema(
  {
    objectID: { type: String, required: true },
    title: { type: String, required: true },
    author: { type: String, required: true },
    url: { type: String, required: false },
    deleted: { type: Boolean, required: false },
  },
  { timestamps: true }
);

module.exports = mongoose.model("feeds", Feed);
