const express = require("express");
const cors = require("cors");
const db = require("./db");
const feedRouter = require("./routes/feed-router");
const FeedController = require("./controllers/feed-ctrl");

const app = express();
const apiPort = 5000;

app.use(cors());

db.on("error", console.error.bind(console, "MongoDB connection error:"));

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.use("/api", feedRouter);

FeedController.seedFeeds();

setInterval(function () {
  FeedController.seedFeeds();
}, 3600000);

app.listen(apiPort, () => console.log(`Server running on port ${apiPort}`));
