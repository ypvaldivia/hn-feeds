const Feed = require("../models/feed-model");
const axios = require("axios");

deleteFeed = async (req, res) => {

  Feed.findOne({ _id: req.params.id }, (err, feed) => {
    if (err) {
      return res.status(404).json({
        err,
        message: "Feed not found!",
      });
    }
    feed.deleted = true;
    feed
      .save()
      .then(() => {
        return res.status(200).json({
          success: true,
          id: feed._id,
          message: "Feed updated!",
        });
      })
      .catch((error) => {
        return res.status(404).json({
          error,
          message: "Feed not updated!",
        });
      });
  });
};

getFeeds = async (req, res) => {
  await Feed.find({deleted: false}, (err, feeds) => {
    if (err) {
      return res.status(400).json({ success: false, error: err });
    }
    if (!feeds.length) {
      return res
        .status(404)
        .json({ success: false, error: `Feeds not founds` });
    }
    return res.status(200).json({ success: true, data: feeds });
  }).catch((err) => console.log(err));
};

seedFeeds = () => {
  try {
    Feed.find({}, (err, feeds) => {
      if (err) {
        return console.log(err);
      }
  
      axios
        .get("https://hn.algolia.com/api/v1/search_by_date?query=nodejs")
        .then(function (response) {
          const hits = response.data.hits;
          hits.map((hit) => {

            const exists = feeds.find((feed) => feed.objectID === hit.objectID);
            console.log(`Object: ${hit.objectID} exists: ${!!exists}`)

            if ((hit.story_title || hit.title) && !exists) {
              const feed = new Feed({
                objectID: hit.objectID,
                title: hit.story_title || hit.title,
                author: hit.author,
                url: hit.story_ulr || hit.url,
                deleted: false,
              });
  
              feed
                .save()
                .then(() => console.log(`Feed created! ${feed._id}`))
                .catch((error) =>
                  console.log(`Error creating feed: ${error.message}`)
                );
            }
          });
        })
        .catch(function (error) {
          console.log(error);
        });
    });
  } catch (error) {
    console.log(error)
  }
  
};

module.exports = {
  deleteFeed,
  getFeeds,
  seedFeeds,
};
