const express = require('express')

const FeedCtrl = require('../controllers/feed-ctrl')

const router = express.Router()

router.delete('/feed/:id', FeedCtrl.deleteFeed)
router.get('/feeds', FeedCtrl.getFeeds)

module.exports = router