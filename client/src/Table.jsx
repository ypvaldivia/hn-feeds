import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import { useTable } from 'react-table'
import axios from "axios";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons'
import moment from 'moment'

const api = axios.create({
  baseURL: "http://localhost:5000/api",
});

const Styles = styled.div`
  padding: 1rem;
  table {
    border-spacing: 0;    
    td {
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid Gainsboro;
      :last-child {
        border-right: 0;
      }
    }
  }
`

function ReactTable({ columns, data, reloadData }) {
  // Use the state and functions returned from useTable to build your UI
  const {
    getTableProps,
    getTableBodyProps,
    // headerGroups,
    rows,
    prepareRow,
  } = useTable({
    columns,
    data,
  })

  const deleteRow = (id) => {
    api
      .delete(`/feed/${id}`)
      .then(() => {
        reloadData()
      })
      .catch((err) => console.log(err))
  }

  // Render the UI for your table
  return (
    <table {...getTableProps()}>
      <tbody {...getTableBodyProps()}>
        {rows.map((row, i) => {
          prepareRow(row)
          return (
            <tr {...row.getRowProps()} style={{ cursor: "pointer" }} >
              {row.cells.map(cell => {
                return <td {...cell.getCellProps()} onClick={() => row.original.url ? window.open(row.original.url, "_blank") : alert("Sorry! this row has not a valid link url")}>{cell.render('Cell')}</td>
              })}
              <td onClick={() => deleteRow(row.original._id)}  ><FontAwesomeIcon icon={faTrashAlt} /></td>
            </tr>
          )
        })}
      </tbody>
    </table>
  )
}

function Table() {
  const columns = React.useMemo(
    () => [
      {
        Header: 'Feeds',
        columns: [
          {
            Header: 'title',
            accessor: 'title',
          },
          {
            Header: 'author',
            accessor: 'author',
          },
          {
            Header: 'time',
            accessor: 'createdAt',
            Cell: props => <div> {moment(props.value).calendar()} </div>
          },
        ],
      },
    ],
    []
  )
  const [data, setData] = useState([]);

  useEffect(() => loadFeeds(), []);

  const loadFeeds = () => api.get(`/feeds`).then((res) => setData(res.data.data)).catch((err) => console.log(err))

  return (
    <Styles>
      {/* <ReactTable columns={columns} data={data} /> */}
      { 
        data !== []
          ? <ReactTable 
              columns={columns} 
              data={data} 
              reloadData={() => loadFeeds()}
            /> 
          : "Sorry! We found no data..."
      }
    </Styles>
  )
}

export default Table
