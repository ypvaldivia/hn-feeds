import React, { Component } from "react";
import "./app.css";
import Table from "./Table";

class App extends Component {
  render() {
    return (
      <div>
        <div className="hero-back">
          <div className="hero-text">
            <h1>HN Feed</h1>
            <p>We love hacker news!</p>
          </div>
        </div>
        <div className="container">
          <Table />
        </div>
      </div>
    );
  }
}

export default App;
